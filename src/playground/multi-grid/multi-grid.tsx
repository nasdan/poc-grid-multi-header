import * as React from 'react';
import {
  MultiGrid as VirtualizedMultiGrid,
  GridCellProps,
} from 'react-virtualized';
import { cellStyles, headerStyles, cellTextStyles } from './multi-grid.styles';
import { Column } from './multi-grid.vm';
import {
  getRowIndex,
  getColumnKey,
  getHeaderText,
} from './multi-grid.business';

interface Props {
  rows: any[];
  columns: Column[];
  width: number;
  fixedRowCount: number;
  columnCount: number;
}

export const MultiGrid: React.FunctionComponent<Props> = props => {
  const { rows, columns, width, fixedRowCount, columnCount } = props;

  const rowCount = columns ? rows.length + fixedRowCount : rows.length;

  return (
    <VirtualizedMultiGrid
      cellRenderer={cellProps => (
        <Cell
          {...cellProps}
          rows={rows}
          columns={columns}
          fixedRowCount={fixedRowCount}
        />
      )}
      columnWidth={width / columnCount}
      columnCount={columnCount}
      height={300}
      rowHeight={40}
      rowCount={rowCount}
      width={width}
      fixedRowCount={fixedRowCount}
      style={{
        border: '1px solid #eee',
      }}
      styleTopRightGrid={{
        borderBottom: '2px solid #eee',
      }}
    />
  );
};

MultiGrid.defaultProps = {
  width: 0,
  fixedRowCount: 0,
};

interface CellProps extends GridCellProps {
  rows: any[];
  columns?: Column[];
  fixedRowCount: number;
}

const Cell: React.FunctionComponent<CellProps> = props => {
  const { rows, columns, fixedRowCount, columnIndex, style } = props;

  const hasColumns = Boolean(columns);
  const isColumn = hasColumns && props.rowIndex <= fixedRowCount - 1;
  const rowIndex = getRowIndex(
    props.rowIndex,
    fixedRowCount,
    hasColumns,
    isColumn
  );

  const columnKey = getColumnKey(rows[rowIndex], columnIndex, isColumn);

  const column = columns.find(
    x => x.columnStart === columnIndex && x.rowIndex === rowIndex
  );

  return isColumn ? (
    column ? (
      <Header
        column={column}
        style={style}
        columns={columns}
        columnStart={columnIndex}
        columnEnd={column.columnEnd}
        rowIndex={rowIndex}
      />
    ) : null
  ) : (
    <div className={cellStyles} style={style}>
      <div className={cellTextStyles}>{rows[rowIndex][columnKey]}</div>
    </div>
  );
};

interface HeaderProps {
  columns: Column[];
  column: Column;
  columnStart: number;
  columnEnd: number;
  rowIndex: number;
  style: React.CSSProperties;
}

const Header: React.FunctionComponent<HeaderProps> = props => {
  const { columns, columnStart, column, rowIndex, style } = props;

  const columnEnd = props.columnEnd || columnStart;

  return (
    <div
      className={headerStyles(column)}
      style={{
        ...style,
        borderLeft: '1px solid #eee',
        marginLeft: '-1px',
        width: `calc(${style.width}px * ${columnEnd - columnStart + 1})`,
      }}
    >
      <div className={cellTextStyles}>
        {getHeaderText(columns, columnStart, rowIndex)}
      </div>
    </div>
  );
};
