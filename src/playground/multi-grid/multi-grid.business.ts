import { Column } from './multi-grid.vm';

export const getRowIndex = (
  currentRowIndex: number,
  fixedRowCount: number,
  hasColumns: boolean,
  isColumn: boolean
) =>
  hasColumns && !isColumn ? currentRowIndex - fixedRowCount : currentRowIndex;

export const getColumnKey = (row, columnIndex: number, isColumn: boolean) =>
  !isColumn ? Object.keys(row)[columnIndex] : '';

export const getHeaderText = (
  columns: Column[],
  columnIndex: number,
  rowIndex: number
) => {
  const column = columns.find(
    c => c.columnStart === columnIndex && c.rowIndex === rowIndex
  );
  return column ? column.name : '';
};
