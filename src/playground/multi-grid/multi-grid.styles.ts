import { css } from 'emotion';
import { Column } from './multi-grid.vm';

export const cellStyles = css`
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 1px solid #eee;
  border-right: 1px solid #eee;
  text-align: left;
  justify-content: flex-start;
`;

export const cellTextStyles = css`
  padding: 10px;
  justify-content: flex-start;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;

export const headerStyles = (column: Column) => css`
  ${cellStyles}
  border-bottom: ${column ? '1px solid #eee' : 'none'};
  flex: ${column.columnEnd - column.columnEnd + 1}
`;
