export interface Column {
  name: string;
  columnStart: number;
  columnEnd?: number;
  rowIndex: number;
}
