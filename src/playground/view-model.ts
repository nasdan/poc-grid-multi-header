export interface Product {
  id: number;
  name: string;
  price: number;
  stock: number;
  categoryName: string;
  categoryDescription: string;
}
