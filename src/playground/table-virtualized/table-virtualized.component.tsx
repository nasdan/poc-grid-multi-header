import * as React from 'react';
import {
  AutoSizer,
  Table,
  Column,
  TableHeaderProps,
  TableCellProps,
  Index,
} from 'react-virtualized';
import * as VM from './table-virtualized.vm';
import Immutable from 'immutable';
import { Cell, CellGroup } from './components';
import { HeaderCell } from './components/header-cell.component';
import { HeaderCellGroup } from './components/header-cell-group.component';

interface Props {
  rows: any[];
  columns: VM.Column[];
  height: number;
  headerHeight: number;
}

export const TableVirtualized: React.FunctionComponent<Props> = props => {
  const { rows, columns, height, headerHeight } = props;

  const [innerList, setInnerList] = React.useState(rows);

  const [sortState, setSortState] = React.useState({
    sortBy: '',
    sortDirection: '',
  });

  const rowGetter = (info: Index) =>
    Immutable.List(innerList)[info.index % rows.length];

  const headerRenderer = (
    column: VM.Column,
    sortedColumn: string,
    props: TableHeaderProps
  ) =>
    !!props && (
      <>
        <HeaderCell
          style={column.headerCellStyle}
          flex={column.flex}
          title={column.title}
          isSortedColumn={column.field === sortedColumn}
          onSort={!!!column.children ? sort({ sortBy: column.field }) : null}
        >
          {!!column.children && (
            <HeaderCellGroup key={column.title}>
              {column.children.map(x => headerRenderer(x, sortedColumn, props))}
            </HeaderCellGroup>
          )}
        </HeaderCell>
      </>
    );

  const cellRenderer = (column: VM.Column, props: TableCellProps) =>
    !!!column.children ? (
      <Cell flex={column.flex} style={column.rowCellStyle}>
        {rows[props.rowIndex][column.field]}
      </Cell>
    ) : (
      <CellGroup flex={column.flex}>
        {column.children.map(x => cellRenderer(x, props))}
      </CellGroup>
    );

  const sort = ({ sortBy }) => () => {
    if (sortState.sortBy === sortBy) {
      setInnerList([...rows.reverse()]);
      setSortState({
        sortBy,
        sortDirection: sortState.sortDirection === 'ASC' ? 'DESC' : 'ASC',
      });
    } else {
      setInnerList([...rows.sort((a, b) => (a[sortBy] > b[sortBy] ? 1 : -1))]);
      setSortState({
        sortBy,
        sortDirection: 'ASC',
      });
    }
  };

  return (
    <AutoSizer disableHeight>
      {({ width }) => (
        <Table
          disableHeader={false}
          headerHeight={headerHeight}
          height={height}
          noRowsRenderer={() => 'No rows'}
          overscanRowCount={5}
          rowHeight={30}
          rowGetter={rowGetter}
          rowCount={rows.length}
          width={width}
        >
          {innerList &&
            columns.map((column, index) => (
              <Column
                key={`${column.title}_${index.toString()}`}
                width={210}
                disableSort
                columnData={column}
                dataKey={`${column.title}_${index.toString()}`}
                headerRenderer={props =>
                  headerRenderer(column, sortState.sortBy, props)
                }
                cellRenderer={props => cellRenderer(column, props)}
                cellDataGetter={props => props.dataKey}
                flexGrow={column.flex || 1}
              />
            ))}
        </Table>
      )}
    </AutoSizer>
  );
};
