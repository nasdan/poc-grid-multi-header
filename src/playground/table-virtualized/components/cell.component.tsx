import * as React from 'react';
import { baseStyles, containerStyles } from './cell.style';

interface CellProps {
  style?: React.CSSProperties;
  flex: number;
}

export const Cell: React.FC<CellProps> = ({ style, flex, children }) => {
  return (
    <div style={{ ...containerStyles(flex) }}>
      <div
        style={{
          ...baseStyles(flex),
          ...style,
        }}
      >
        {children}
      </div>
    </div>
  );
};
