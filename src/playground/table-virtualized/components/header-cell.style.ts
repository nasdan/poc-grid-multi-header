import { CSSProperties } from 'react';

export const baseStyles = (): CSSProperties => ({
  justifyContent: 'flex-end',
  display: 'flex',
  flex: 1,
  flexDirection: 'column',
  padding: 6,
});

export const containerStyles = (flex: number): CSSProperties => ({
  flex: flex > 0 ? flex : 1,
  display: 'flex',
  flexDirection: 'column',
  border: '1px solid gray',
  margin: '-1px -1px -1px -1px',
  overflow: 'hidden',
});

export const titleStyle = (hasChildren: boolean): CSSProperties => ({
  marginBottom: hasChildren ? 12 : 0,
  textOverflow: 'ellipsis',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  cursor: hasChildren ? 'inherit' : 'pointer',
});
