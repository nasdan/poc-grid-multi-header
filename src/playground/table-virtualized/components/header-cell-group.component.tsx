import * as React from 'react';

interface Props {
  style?: React.CSSProperties;
}

export const HeaderCellGroup: React.FC<Props> = ({ children, style }) => (
  <div
    style={{
      display: 'flex',
      margin: -6,
      ...style,
    }}
  >
    {children}
  </div>
);
