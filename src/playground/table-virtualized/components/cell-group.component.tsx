import * as React from 'react';

interface Props {
  flex: number;
}

export const CellGroup: React.FC<Props> = ({ flex, children }) => (
  <div style={{ flex: flex > 0 ? flex : 1, display: 'flex' }}>{children}</div>
);
