import * as React from 'react';
import { baseStyles, containerStyles, titleStyle } from './header-cell.style';

interface Props {
  title: React.ReactNode;
  style?: React.CSSProperties;
  flex: number;
  onSort: () => void;
  isSortedColumn: boolean;
}

export const HeaderCell: React.FC<Props> = ({
  style,
  flex,
  title,
  onSort,
  isSortedColumn,
  children,
}) => {
  return (
    <div style={{ ...containerStyles(flex) }}>
      <div
        style={{
          ...baseStyles(),
          ...style,
        }}
      >
        <div style={{ ...titleStyle(!!children) }} onClick={onSort}>
          {title}{' '}
          {isSortedColumn && <span style={{ fontSize: '80%' }}>🠓🠕</span>}
        </div>
        {children}
      </div>
    </div>
  );
};
