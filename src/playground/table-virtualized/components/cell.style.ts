import { CSSProperties } from 'react';

export const baseStyles = (flex: number): CSSProperties => ({
  flex: flex > 0 ? flex : 1,
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  padding: 6,
});

export const containerStyles = (flex: number): CSSProperties => ({
  flex: flex > 0 ? flex : 1,
  margin: -1,
  overflow: 'hidden',
  textOverflow: 'ellipsis',
});
