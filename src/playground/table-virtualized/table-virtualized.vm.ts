export interface Column {
  title: string;
  field?: string;
  flex?: number;
  headerCellStyle?: React.CSSProperties;
  rowCellStyle?: React.CSSProperties;
  children?: Column[];
}
