import * as React from 'react';
import { AutoSizer, GridCellProps } from 'react-virtualized';
import { MultiGrid } from './multi-grid/multi-grid';
import { Product } from './view-model';
import { mockProducts } from './mock-data';

export const Playground: React.FunctionComponent = props => {
  const [products, setProducts] = React.useState<Product[]>([]);

  React.useEffect(() => {
    setProducts(mockProducts);
  }, []);

  return (
    <AutoSizer disableHeight>
      {({ width }) => (
        <MultiGrid
          rows={products}
          width={width}
          columns={[
            { name: 'ID', columnStart: 0, rowIndex: 2 },
            {
              name: 'Product Information',
              columnStart: 1,
              columnEnd: 3,
              rowIndex: 0,
            },
            { name: 'Name', columnStart: 1, rowIndex: 2 },
            { name: 'Unit', columnStart: 2, columnEnd: 3, rowIndex: 1 },
            { name: 'Price', columnStart: 2, rowIndex: 2 },
            { name: 'In Stock', columnStart: 3, rowIndex: 2 },
            { name: 'Category', columnStart: 4, columnEnd: 5, rowIndex: 1 },
            { name: 'Name', columnStart: 4, rowIndex: 2 },
            { name: 'Description', columnStart: 5, rowIndex: 2 },
          ]}
          fixedRowCount={3}
          columnCount={6}
        />
      )}
    </AutoSizer>
  );
};
